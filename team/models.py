from django.db import models
from django.utils import timezone
import datetime
from datetime import timedelta

class Word(models.Model):
    kanji = models.CharField(max_length=30)
    search_by = models.CharField(max_length=200)
    search_by_id = models.CharField(max_length=200)
    search_time = models.DateTimeField(default= timezone.now)
    english_meaning = models.CharField(max_length=200)
    level = models.CharField(max_length=20, null=True)
    reading = models.CharField(max_length=200)
    image_url = models.CharField(max_length=500, null=True, blank=True, default = "https://bitsofco.de/content/images/2018/12/broken-1.png")

    def __str__(self):
        return self.kanji

class Leaderboard(models.Model):
    username = models.CharField(max_length=40)
    student_id = models.CharField(max_length=40)
    correct = models.IntegerField(default=0)
    total = models.IntegerField(default=0)

    def __str__(self):
        return self.username

    def get_winrate(self):
        if self.total == 0:
            return 0
        winrate = self.correct/self.total
        return winrate


    
    
